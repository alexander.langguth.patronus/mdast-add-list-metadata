var visitWithParents = require('unist-util-visit-parents');

function addListMetadata() {
  return function(ast){
	  visitWithParents(ast, 'list', function(listNode, parents){
		  var depth = 0;
		  for (var elem in parents){
			  if (parents[elem].type === 'list') depth += 1;
          }
		  for (var i = 0, n = listNode.children.length; i < n; i++) {
			  var child = listNode.children[i];
			  child.index = i;
			  child.ordered = listNode.ordered;
		  }
		  listNode.depth = depth;
	  });
	  return ast;
  };
}

module.exports = addListMetadata;
